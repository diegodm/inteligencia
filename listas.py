from nodos import NodoLDE


class LDE:
    def __init__(self):
        self.cab = None
        self.tamano = 0
        self.cursor = None

    def es_vacia(self):
        """Este metodo comprueba si esta lista esta vacia o no
        :rtype:True si la lista es vacia. False en caso contrario
        """
        if self.cab is None:
            return True
        else:
            return False

    def __eq__(self,otro):
        if type(otro.dato) == type(self.cab.dato):
            if otro.dato == self.cab.dato:
                return True
            else:
                return False
    def posicionar(self, pos):
        nodo_actual = self.cab
        if pos <= self.tamano and type(5) == pos:
            counter = 0
            while nodo_actual.sig is not None:
                if counter == pos:
                    pass
                nodo_actual = nodo_actual.sig
                counter = counter + 1
        else:
            pass




    def recorrer(self,inverse=False, sep=""):
        """
        este metodo recorre cada nodo de la lista y lo imprime
        """
        if self.cab is None:
            return None
        if inverse is False:
            if self.cab is not None:
                nodo_actual = self.cab
                concatenar = str(self.cab.dato)
                while nodo_actual.sig is not None:
                    nodo_actual = nodo_actual.sig
                    concatenar = concatenar + sep + str(nodo_actual.dato)

                return concatenar
            else:
                return None
        else:
            nodo_actual = self.cab

            while nodo_actual.sig is not None:
                nodo_actual = nodo_actual.sig
            concatenar = str(nodo_actual.dato)
            while nodo_actual.ant is not None:
                nodo_actual = nodo_actual.ant
                concatenar = concatenar + sep + str(nodo_actual.dato)
            return concatenar


    def encontrar(self,dato):
        """
                 Metodo que permite encontar el objeto completo
        """
        if type(self.cab.dato) == type(dato):
            nodo_actual = self.cab
            while nodo_actual.sig is not None:
                if nodo_actual.dato == dato:
                    return nodo_actual
                nodo_actual = nodo_actual.sig
        else:
            return None

    def atras(self):
        if self.cursor.ant is None:
            return self.cursor
        else:
            if cursor.ant is None:
                return cursor
            cursor = cursor.ant
            return cursor


    def adelante(self):
       if self.cursor.sig is None:
           return self.cursor
       else:
        if self.cursor.sig is not None:
               self.cursor = cursor.sig
       return self.cursor

    def __len__(self):
        """
        retorna el tamaño de la lista
        """
        return self.tamano

    def agregar(self, nuevo_dato):
        """
        El metodo agregar tiene que devolver un valor boleano(true si se puede,
        false sino)
        """
        nuevo_nodo = NodoLDE(nuevo_dato)
        if self.es_vacia():
            self.cab = nuevo_nodo
            self.cursor = self.cab
            self.tamano = self.tamano + 1
            return True
        else:
            nodo_actual = self.cab
            if type(self.cab.dato) == type(nuevo_dato):
                while nodo_actual.sig is not None:
                    nodo_actual = nodo_actual.sig

                nodo_anterior = nodo_actual
                nodo_actual.sig = nuevo_nodo
                nuevo_nodo.ant = nodo_anterior
                self.tamano = self.tamano + 1
                return True
            else:
                return False

    def remover(self, item, por_pos=False, todo=False):
        nodo_actual = self.cab
        if nodo_actual is None:
            return False
        if por_pos is False and todo is False:
            if type(item) ==  type(self.cab.dato):
                        while nodo_actual.sig is not None:
                            if nodo_actual.dato == item:

                                referencia_anterior = nodo_actual.ant
                                referencia_siguiente = nodo_actual.sig
                                if referencia_anterior is None:

                                    referencia_siguiente.ant = None
                                    self.cab = referencia_siguiente
                                    self.tamano = self.tamano - 1

                                    return True
                                else:
                                    referencia_siguiente.ant = referencia_anterior
                                    referencia_anterior.sig = referencia_siguiente
                                    self.tamano = self.tamano - 1
                                    return True
                            nodo_actual = nodo_actual.sig
                        return False


            else:
                return False
        else:

            if por_pos is False and todo is True:
                if type(item) ==  type(self.cab.dato):
                    while nodo_actual.sig is not None:
                        if nodo_actual.dato == item:
                            referencia_anterior = nodo_actual.ant
                            referencia_siguiente = nodo_actual.sig
                            if referencia_anterior is None:
                                referencia_siguiente.ant = None
                                self.cab = referencia_siguiente
                                self.tamano = self.tamano - 1
                                if nodo_actual.sig is None:
                                    return True
                            else:
                                referencia_siguiente.ant = referencia_anterior
                                referencia_anterior.sig = referencia_siguiente
                                self.tamano = self.tamano - 1
                                if nodo_actual.sig is None:
                                    return True
                            nodo_actual = nodo_actual.sig
                    return False

            if por_pos is True and type(5) == type(item):
                counter = 0
                while nodo_actual.sig is not None:
                        if counter == item:

                            referencia_anterior = nodo_actual.ant
                            referencia_siguiente = nodo_actual.sig
                            if referencia_anterior is None:
                                referencia_siguiente.ant = None
                                self.cab = referencia_siguiente
                                self.tamano = self.tamano - 1
                                if nodo_actual.sig is None:
                                    return True
                            else:
                                referencia_siguiente.ant = referencia_anterior
                                referencia_anterior.sig = referencia_siguiente
                                self.tamano = self.tamano - 1
                                if nodo_actual.sig is None:
                                    return True
                        nodo_actual = nodo_actual.sig
                        counter = counter + 1
                        print(str(nodo_actual) + "NUMERO")




    def __iter__(self):
        nodo_actual = self.cab
        while nodo_actual is not None:
            yield(nodo_actual.dato)
            nodo_actual = nodo_actual.sig



if __name__ == '__main__':


    lista = LDE()
    lista.agregar(5)
    lista.agregar(6)
    lista.agregar(7)
    for m in lista:
        print(m)
    #hola = NodoLDE(5)
    #print(str(lista.agregar(hola)))

    """print(str(lista.cab))

    print(str(lista.agregar(5)))
    print(str(lista.agregar(4)))

    print(str(lista.agregar(3)))
    print(str(lista.agregar(2)))
    print(lista.recorrer(False, "+") + "sin eliminar")
    print(lista.remover(2, True, False))
    print(lista.recorrer(False, "+") + "eliminado")
    print(str(lista.remover(5, False, False)) + "5 eliminado?")
    print(lista.recorrer(False, "+") + "eliminado")

    print(lista.encontrar(5))
    print("tamaño " + str(len(lista)))
    print(str(lista.adelante))"""
