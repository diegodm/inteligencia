import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic
from ventana import *
import copy
import math
from  listasordenada import *

class almSum:
    def __init__(self, piezas=0):

        self.piezas = piezas

class almTer:
    def __init__(self):

        self.piezas = 0

    def enviarpiezas(self):

        self.piezas = self.piezas + 1

class tornos:
    def __init__(self):

        self.ocp = False
        self.ti = 0
        self.tf = 0

class robot:
    def __init__(self):
        self.ocp = False
        self.ti = 0
        self.tf = 0
        self.dtn = ""


class fresas:
    def __init__(self):

        self.ocp = False
        self.ti = 0
        self.tf = 0

class Estado:

    def __init__(self,alm,almter,r1,r2,t1,t2,fr):
        self.R1 = r1
        self.R2 = r2
        self.FR = fr
        self.T2 = t2
        self.T1 = t1
        self.almtermi = almter
        self.almsumi  = alm
        self.ponderacion = 0
        self.subestadoa = None
        self.subestadob = None
        self.subestadoc = None
        self.subestadod = None
        self.subestadoe = None
        self.subestadof = None
        self.subestadog = None
        self.subestadoh = None
        self.profundidad = 0
        self.padre = None
        self.visitado = False
        #self.vector
    def __lt__(self,other):
        if self.ponderacion < other.ponderacion:
            return True
        else:
            return False

    def __gt__(self,other):
        if self.ponderacion > other.ponderacion:
            return True
        else:
            return False

    def __eq__(self,other):
        if self.ponderacion == other.ponderacion:
            return True
        else:
            return False

    def __ge__(self,other):
        if self.ponderacion >= other.ponderacion:
            return True
        else:
            return False

    def __le__(self,other):
        if self.ponderacion <= other.ponderacion:
            return True
        else:
            return False


class funcionHueristica:
    def __init__(self,almacen, terminados, robot1, robot2, torno1,torno2, fresa):
        self.raiz = Estado(almacen, terminados, robot1, robot2, torno1,torno2, fresa)
        self.Caminos = LDE()

    def crear_estado(self):
        self.raiz = self.__crear_estado(self.raiz)

    def __crear_estado(self, subestado):
        if(subestado is None):
            return False
        print(type(subestado),"soy un metodo")
        if(subestado.almtermi.piezas < subestado.almsumi.piezas ):
            print(subestado.almsumi.piezas)
            subestado.profundidad = subestado.profundidad + 1
            print("inicio profundidad",subestado.profundidad)
            subestado.subestadoa = self.piezasalmt1(copy.deepcopy(subestado),subestado)
            subestado.subestadob = self.piezasalmt2(copy.deepcopy(subestado),subestado)
            subestado.subestadoc = self.procesart1(copy.deepcopy(subestado),subestado)
            subestado.subestadod = self.procesart2(copy.deepcopy(subestado),subestado)
            subestado.subestadoe = self.T1afresa(copy.deepcopy(subestado),subestado)
            subestado.subestadof = self.T2afresa(copy.deepcopy(subestado),subestado)
            subestado.subestadog = self.ProcesarFresa(copy.deepcopy(subestado),subestado)
            subestado.subestadoh = self.Fresa_AlmTerminado(copy.deepcopy(subestado),subestado)
            self.calcular_tiempomenor(subestado)
            self.__crear_estado(self.Caminos.primero())
        else:
            camino_final = self.encontrar_camino()


    def calcular_tiempomenor(self,subestado):
        self.Caminos.agregar(subestado.subestadoa)
        self.Caminos.agregar(subestado.subestadob)
        self.Caminos.agregar(subestado.subestadoc)
        self.Caminos.agregar(subestado.subestadod)
        self.Caminos.agregar(subestado.subestadoe)
        self.Caminos.agregar(subestado.subestadof)
        self.Caminos.agregar(subestado.subestadog)
        self.Caminos.agregar(subestado.subestadoh)
    def encontrar_camino(self,subestado):



    def ponderar(self,subestado):
        if subestado is None:
            return
        print(subestado.almsumi.piezas )
        print(subestado.R1.ocp )
        print(subestado.R1.ti )
        print(subestado.R1.tf)
        print(subestado.R1.dtn )
        print(subestado.R2.ocp )
        print(subestado.R2.ti)
        print(subestado.R2.tf)
        print(subestado.R2.dtn )
        print(subestado.T1.ocp )
        print(subestado.T1.ti )
        print(subestado.T1.tf)
        print(subestado.T2.ocp )
        print(subestado.T2.ti )
        print(subestado.T2.tf)
        print(subestado.FR.ocp )
        print(subestado.FR.ti )
        print(subestado.FR.tf)
        print(subestado.almtermi.piezas )
        print("_______________________________")

    def piezasalmt1(self,subestado,padre=None):
        if subestado.almsumi.piezas > 0 and subestado.R1.ocp is False and subestado.T1.ocp is False:
            subestado.almsumi.piezas = subestado.almsumi.piezas - 1
            subestado.R1.ocp = True
            subestado.R1.ti = subestado.R1.tf
            subestado.R1.tf = subestado.R1.ti + rtorno1
            subestado.R1.dtn = "A"
            subestado.ponderacion = subestado.R1.tf
            print(subestado.almsumi.piezas )
            print(subestado.R1.ocp )
            print(subestado.R1.ti )
            print(subestado.R1.tf)
            print(subestado.R1.dtn )
            subestado.padre = padre
            return subestado
        else:
            return None #retorna false si no se pudo enviar enviarpiezas

    def piezasalmt2(self,subestado,padre=None):
        if subestado.almsumi.piezas > 0 and subestado.R1.ocp is False and subestado.T2.ocp is False:
            subestado.almsumi.piezas = subestado.almsumi.piezas - 1
            subestado.R1.ocp = True
            subestado.R1.ti = subestado.R1.tf
            subestado.R1.tf = subestado.R1.ti + rtorno2
            subestado.R1.dtn = "B"
            print("entre b")
            subestado.ponderacion = subestado.R1.tf
            subestado.padre = padre
            return subestado
        else:
            return None #retorna false si no se pudo enviar enviarpiezas

    def procesart1(self,subestado,padre=None):
        if subestado.R1.ocp is True and subestado.R1.dtn == "A" and subestado.T1.ocp is False:
           subestado.R1.ocp = False
           subestado.T1.ocp = True
           subestado.T1.ti = robot1.tf
           subestado.T1.tf = subestado.T1.ti + trn1
           subestado.ponderacion = subestado.T1.tf
           subestado.padre = padre
           print("entre c")
           return subestado
        else:
            return None

    def procesart2(self,subestado,padre=None):
        if subestado.R1.ocp is True and subestado.R1.dtn == "B" and subestado.T2.ocp is False:
           subestado.R1.ocp = False
           subestado.T2.ocp = True
           subestado.T2.ti = robot1.tf
           subestado.T2.tf = subestado.T2.ti + trn2
           subestado.ponderacion = subestado.T2.tf
           subestado.padre = padre
           print("entre d")
           return subestado
        else:
           return None

    def validarTiempo(self, t1, t2):
        if t1 <= t2 :
            return t2
        else:
            return t1

    def T1afresa(self,subestado,padre=None):
        if subestado.T1.ocp is True and subestado.R1.ocp is False and (subestado.FR.ocp is False or ( math.fabs(self.validarTiempo(subestado.R1.tf, subestado.T1.tf) - subestado.FR.tf ) <= trn1)):
            subestado.T1.ocp = False
            subestado.R1.ocp = True
            subestado.R1.ti = self.validarTiempo(subestado.R1.tf, subestado.T1.tf)
            t =  (subestado.FR.tf - t1fresa)
            subestado.R1.ti = self.validarTiempo(subestado.R1.ti,t)
            subestado.R1.tf = subestado.R1.ti + t1fresa
            subestado.R1.dtn = "C"
            subestado.ponderacion = subestado.R1.tf
            print("entre e")
            subestado.padre = padre
            return subestado
        else:
            return None

    def T2afresa(self,subestado,padre=None):
        if subestado.T2.ocp is True and subestado.R1.ocp is False and (subestado.FR.ocp is False or ( math.fabs(self.validarTiempo(subestado.R1.tf, subestado.T2.tf) - subestado.FR.tf ) <= trn2)):
            subestado.T2.ocp = False
            subestado.R1.ocp = True
            subestado.R1.ti = self.validarTiempo(subestado.R1.tf, subestado.T2.tf)
            t = (subestado.FR.tf - t2fresa)
            subestado.R1.ti = self.validarTiempo(subestado.R1.ti, t)
            subestado.R1.tf = subestado.R1.ti + t2fresa
            subestado.R1.dtn = "C"
            subestado.ponderacion = subestado.R1.tf
            subestado.padre = padre
            return subestado
        else:
            return None
    def ProcesarFresa(self, subestado,padre=None):
        if subestado.FR.ocp is False and subestado.R1.dtn == "C":
            subestado.R1.ocp = False
            subestado.FR.ocp = True
            subestado.FR.ti = subestado.R1.tf
            subestado.FR.tf = subestado.FR.ti + tfresa
            subestado.ponderacion = subestado.FR.tf
            subestado.padre = padre
            return subestado
        else:
            return None

    def Fresa_AlmTerminado(self, subestado,padre=None):
        if subestado.R2.ocp is False and subestado.FR.ocp is True:
            subestado.FR.ocp = False
            subestado.R2.ocp = True
            subestado.R2.ti = subestado.FR.tf
            subestado.R2.tf = subestado.R2.ti + fter
            subestado.almtermi.piezas = subestado.almtermi.piezas + 1
            subestado.R2.ocp = False
            subestado.ponderacion = subestado.R2.tf
            subestado.padre = padre
            return subestado
        else:
            return None



if __name__ == "__main__": #esta es la funcion main equivalente a int main (){}
    rtorno1 = 1
    rtorno2 = 2
    fter = 3
    t1fresa = 4
    t2fresa = 5
    tfresa = 6
    trn1 = 7
    trn2 = 8
    piezas = 5
    tiempo = 0
    robot1 = robot()
    robot2 = robot()
    torno1 = tornos()
    torno2 = tornos()
    fresa = fresas()
    almacen = almSum(piezas)
    terminados = almTer()
    grafo = funcionHueristica(almacen, terminados, robot1, robot2, torno1,torno2, fresa)
    grafo.crear_estado()
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
