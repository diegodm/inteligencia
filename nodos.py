class NodoLDE:

    def __init__(self, dato):
        self.dato = dato
        self.sig = None
        self.ant = None

    def __str__(self):

        return str(self.dato)


class NodoLSE:

    def __init__(self, dato):
        self.dato = dato
        self.sig = None

    def __str__(self):

        return str(self.dato)


class NodoQ_S:

    def __init__(self, dato=None):

        self.dato = dato
        self.sig = None

    def __str__(self):

        return str(self.dato)
