from validación import validacion
from nodos import NodoQ_S


class Queue:
    def __init__(self):
        self.tamanio = 0
        self.primero = None
        self.ultimo = None

    def encolar(self, nuevo_dato):
        nuevo_nodo = NodoQ_S(nuevo_dato)
        if self.ultimo is None and nuevo_dato is not None:
            self.primero = nuevo_nodo
            self.ultimo = nuevo_nodo
            self.tamanio = self.tamanio + 1
            return True
        elif validacion(self.ultimo.dato, nuevo_dato) is True and nuevo_dato is not None:
            self.ultimo.sig = nuevo_nodo
            self.ultimo = nuevo_nodo
            self.tamanio = self.tamanio + 1
            return True
        else:
            return False

    def recorrer(self):
        nodo_actual = self.primero
        while nodo_actual is not None:
            print(nodo_actual.dato)
            nodo_actual = nodo_actual.sig

    def desencolar(self):
        if self.es_vacia is not True:
            dato = self.primero.dato
            if self.primero.sig is None:
                self.tamanio = self.tamanio - 1
                self.primero = None
                self.ultimo = None
                return dato
            else:
                self.primero = self.primero.sig
                self.tamanio = self.tamanio - 1

            return dato
        else:
            return None
    def es_vacia(self):
        if self.primero is None:
            return True
        else:
            return False
        pass
    def frente(self):
        return self.primero
    def __len__(self):
        var = self.tamanio
        return var

    def __iter__(self):
        nodo_actual = self.primero
        while nodo_actual is not None:
            yield(nodo_actual.dato)
            nodo_actual = nodo_actual.sig

if __name__ == "__main__":

    a = Queue()
    print(a.encolar(5))
    print("recorrer")
    a.recorrer()
    print(a.encolar(4))
    print(len(a))
    print(a.encolar("moneda"))
    print(len(a))
    print(a.encolar(2))
    print(len(a))
    print(a.encolar(1))
    print(len(a))
    print("recorrer")
    a.recorrer()
    print("tamaño de la cola")
    print(len(a))
    for e in a:
        print(e)
